/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

YAHOO.namespace( 'amostudio' );
(function () {




    var Dom = YAHOO.util.Dom,
    Event = YAHOO.util.Event,
    Lang = YAHOO.lang,
    UA = YAHOO.env.ua


    function oc(id,cv) {
        if (document.all) {
            document.all[''+id+''].innerHTML = cv;
        } else {
            rng = document.createRange();
            el = document.getElementById(''+id+'');
            rng.setStartBefore(el);
            htmlFrag = rng.createContextualFragment(cv)
            while(el.hasChildNodes()) el.removeChild(el.lastChild);
            el.appendChild(htmlFrag);
        }
    }

    YAHOO.amostudio.WMedia= function(id,w,h,playlist){

        this.id = id;
        var defaultWidth = w;
        var defaultHeight = h;
        var _embedversion = 3;


        var currentlyPlaying="";

        var redirect = "";
        var sq = document.location.search.split("?")[1] || "";
        var altTxt = "This content requires the Microsoft Windows Media Plugin. <a href='http://www.microsoft.com/windows/windowsmedia/download/'>Download Windows Media Player</a>.";
        var params = new Object();

        /* get value of querystring param */
        var _getQueryParamValue =  function (param) {
            var q = document.location.search;
            var detectIndex = q.indexOf(param);
            var endIndex = (q.indexOf("&", detectIndex) != -1) ? q.indexOf("&", detectIndex) : q.length;
            if(q.length > 1 && detectIndex != -1) {
                return q.substring(q.indexOf("=", detectIndex)+1, endIndex);
            } else {
                return "";
            }
        }

        var addParam = function(name, value) {
            params[name] = value;
        }

        var getParams = function() {
            return params;
        }

        var getParam = function(name) {
            return params[name];
        }

        // depricated used only with writePlayer instead of createPlayer
        var getParamTags = function() {
            var paramTags = "";
            for (var param in getParams()) {
                paramTags += '<param name="' + param + '" value="' + getParam(param) + '" />';
            }
            if (paramTags == "") {
                paramTags = null;
            }
            return paramTags;
        }


        var _embedCode = function (mov,w,h) {
            var _w,_h
            if(w && w!=undefined && w>0){
                _w = w;
            }else {
                _w = defaultWidth;
            }

            if(h && h!=undefined && h>0){
                _h= h;
            }else {
                _h = defaultHeight;
            }

            currentlyPlaying = mov;


              // TODO depricate this
            switch (_embedversion) {
                case 1:
                    return  _embedCodev1(mov,_w,_h);
                 break;
                  case 2:
                       return  _embedCodev2(mov,_w,_h);
                 break;
                default:
                     return  _embedCodev3(mov,_w,_h);
                    break;
            }
        }

         // depricated
        var _embedCodev1= function(mov,_w,_h) {
            var wmpHTML = "";
            wmpHTML += '<object   classid="CLSID:22D6F312-B0F6-11D0-94AB-0080C74C7E95" width="' + _w+ '" height="' + _h + '"  id="' + id+ '">';
            addParam("src", mov);
            wmpHTML += '<embed type="application/x-mplayer2" pluginspage="http://www.microsoft.com/Windows/MediaPlayer/" src="'
            + mov + '" width="' + _w + '" height="' + _h + '" id="' + id + '"';
            for (var param in getParams()) {
                wmpHTML += ' ' + param + '="' + getParam(param) + '"';
            }
            wmpHTML += '></embed>';
            if (getParamTags() != null) {
                wmpHTML += getParamTags();
            }
            wmpHTML += '</object>';
            return wmpHTML;
        }

       // depricated
        var _embedCodev2= function(mov,_w,_h) {
            var wmpHTML = "";
            wmpHTML += '<object type="application/x-oleobject"  standby="Loading Microsoft Windows Media Player components..."  codebase="http://activex.microsoft.com/activex/controls/mplayer/en/nsmp2inf.cab#Version=5,1,52,701" classid="CLSID:6BF52A52-394A-11d3-B153-00C04F79FAA6"  width="' + _w+ '" height="' + _h + '"  id="' + id+ '">';
            addParam("src", mov);
            wmpHTML += '<embed pluginspage="http://microsoft.com/windows/mediaplayer/ en/download/" type="application/x-ms-wmp" src="'
            + mov + '" width="' + _w + '" height="' + _h + '" id="' +id + '"';
            for (var param in getParams()) {
                wmpHTML += ' ' + param + '="' + getParam(param) + '"';
            }
            wmpHTML += '></embed>';
            if (getParamTags() != null) {
                wmpHTML += getParamTags();
            }
            wmpHTML += '</object>';
            return wmpHTML;
        }


        var _embedCodev3 = function (mov,_w,_h){

            if(YAHOO.env.ua.ie){
                var myObject = document.createElement('object');
                myObject.id = id;
                myObject.standby = "Loading Microsoft Windows Media Player components...";
                myObject.codebase = "http://activex.microsoft.com/activex/controls/mplayer/en/nsmp2inf.cab#Version=5,1,52,701";
                myObject.classid = "CLSID:6BF52A52-394A-11d3-B153-00C04F79FAA6";
                myObject.width = _w;
                myObject.height = _h;
                myObject.URL = mov;
                myObject.setAttribute('src',mov);
                myObject.setAttribute('type',"application/x-oleobject");

                //TODO this is a little redundant
               for (var i in params){
                            myObject.setAttribute(i,params[i]);
               }

                return myObject;


            } else {

                var embed = document.createElement('embed');
                embed.setAttribute('pluginspage',"http://microsoft.com/windows/mediaplayer/ en/download/");
                embed.setAttribute('type',"application/x-ms-wmp");
                embed.setAttribute('width',_w);
                embed.setAttribute('height',_h);
                embed.setAttribute('src',mov);
                embed.setAttribute('id',id);
                embed.setAttribute('name',id);
                // myObject.appendChild(embed);

                //TODO this is a little redundant
                 for (var i in params){
                            embed.setAttribute(i,params[i]);
               }
                return embed;
            }

        }


        var getVariablePairs = function() {
            var variablePairs = new Array();
            for (var name in getVariables()) {
                variablePairs.push(name + "=" + escape(getVariable(name)));
            }
            if (variablePairs.length > 0) {
                return variablePairs.join("&");
            }
            else {
                return null;
            }
        }

       // depricated
        var  writePlayer = function(elementId,mov,_w,_h) {
            _embedversion=1;
            if(isWMPInstalled() || doDetect=='false') {
                if (elementId) {
                    oc(elementId, _embedCode(mov,_w,_h));
                } else {
                    document.write(_embedCode(mov,_w,_h));
                }
            } else {
                if (redirect != "") {
                    document.location.replace(redirect);
                } else {
                    if (elementId) {
                        document.getElementById(elementId).innerHTML = altTxt +""+ bypassTxt;
                    } else {
                        document.write(altTxt +""+ bypassTxt);
                    }
                }
            }
        }


        // TODO rewrite once we Extends Element
        var loadDefaults = function (){
            _player.uiMode="full";
            _player.stretchToFit=true;
            _player.settings.autoStart=true;
            _player.settings.mute=false;
            _player.settings.setMode("loop",true);
            _player.settings.volume=50;
            _player.settings.balance=0;
            _player.settings.playCount=10;

        // alert(_player.object.settings.mute);
        // alert(_player.settings.mute);
        }

        var  createPlayer = function(elementId,mov,_w,_h) {
            if(isWMPInstalled() || doDetect=='false') {
                 _embedversion=3;
                _player =_embedCode(mov,_w,_h);
                if (elementId) {
                      var e = Dom.get(elementId);
                    _player =  e.appendChild(_player);
                }
               return  _player;
            } else {
                if (redirect != "") {
                    document.location.replace(redirect);
                } else {
                    if (elementId) {
                        document.getElementById(elementId).innerHTML = altTxt +""+ bypassTxt;
                    } else {
                        document.write(altTxt +""+ bypassTxt);
                    }
                }
            }
        }

        var isWMPInstalled = function () {
            var wmpInstalled = false;
            wmpObj = false;
            if (navigator.plugins && navigator.plugins.length) {
                for (var i=0; i < navigator.plugins.length; i++ ) {
                    var plugin = navigator.plugins[i];
                    if (plugin.name.indexOf("Windows Media Player") > -1) {
                        wmpInstalled = true;
                    }
                }
            } else {
                execScript('on error resume next: wmpObj = IsObject(CreateObject("MediaPlayer.MediaPlayer.1"))','VBScript');
                wmpInstalled = wmpObj;
            }
            return wmpInstalled;
        }

        var _player = null;
        var _getPlayer = function (force) {
            if(force ||  YAHOO.lang.isNull(_player)){
                _player = Dom.get(id);
            }
            return _player;
        }

        // boolean
        var  stretchToFit = function (value) {
            _player.stretchToFit=value;
        }

        var updateDims = function (_w,_h){
                _player.setAttribute('width',_w);
                _player.setAttribute('height',_h);
        }

        //boolean
        var  autoStart = function (value) {
            _player.settings.autoStart=value;
        }

        // boolean
        var  mute = function (value) {
            if(value &&  YAHOO.lang.isBoolean(value)) {
            _player.settings.mute=value;
            } else {
                _player.settings.mute = !_player.settings.mute;
            }
        }

        // boolean
        var  setLoopMode= function (value) {

            _player.settings.setMode("loop",value);
        }

        // 0-100
        var  volume = function (value) {
            if(YAHOO.lang.isNumber(value)) {
                _player.settings.volume = value;
            }
            return _player.settings.volume;
        }

        // -100 to 100
        var  balance = function (value) {
            _player.settings.balance = value;
        }

        // none,mini,full
        var  uiMode = function (value) {
            if(!YAHOO.lang.isString(value)) {
                _player.uiMode = value;
            }
            return _player.uiMode =value
        }

        var pause = function () {
            _player.controls.pause();
        }

        var stop = function () {
            _player.controls.stop();
        }

        var play = function (value) {
            if(YAHOO.lang.isString(value)){
                _player.URL = value;
            }
            _player.controls.play();
        }

         var fastForward = function () {
                 if (_player.controls.isAvailable('FastForward')) {
                  _player.controls.fastForward();
             }else {
                alert("FastForward not available");
                }
        }

        var fastReverse = function () {
                 if (_player.controls.isAvailable('FastReverse')) {
                  _player.controls.fastReverse();
             }else {
                alert("FastReverse not available");
                }
        }

        return {
            id:id,
            embedCode:_embedCode,
            isWMPInstalled:isWMPInstalled,
            writePlayer:writePlayer,
            getPlayer:_getPlayer,
            createPlayer:createPlayer,
            loadDefaults:loadDefaults,
            stretchToFit:stretchToFit,
            uiMode:uiMode,
            volume:volume,
            balance:balance,
            setLoopMode:setLoopMode,
            mute:mute,
            autoStart:autoStart,
            play:play,
            stop:stop,
            pause:pause,
            fastForward:fastForward,
            fastReverse:fastReverse,
            updateDims:updateDims,
            currentlyPlaying:currentlyPlaying
        // public
        };

    };

})();
YAHOO.register('amostudio.WMedia', YAHOO.amostudio.WMedia, {
    version: "0.1.01",
    build: '3'
});