/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

YAHOO.namespace( 'amostudio' );
(function () {

    var Dom = YAHOO.util.Dom,
    Event = YAHOO.util.Event,
    Lang = YAHOO.lang,
    UA = YAHOO.env.ua

    YAHOO.amostudio.WMPlayer= function(id,w,h){

        var currentlyPlaying ="";

        var id = id;
        var width = w;
        var height = h;
        var redirect = "";
        var sq = document.location.search.split("?")[1] || "";
        var altTxt = "This content requires the Microsoft Windows Media Plugin. <a href='http://www.microsoft.com/windows/windowsmedia/download/'>Download Windows Media Player</a>.";
        var bypassTxt = "<p>Already have Windows Media Player? <a href='?detectwmp=false&"+ sq +"'>Click here.</a></p>";
        var params = new Object();


        /* get value of querystring param */
        var getQueryParamValue =  function (param) {
            var q = document.location.search;
            var detectIndex = q.indexOf(param);
            var endIndex = (q.indexOf("&", detectIndex) != -1) ? q.indexOf("&", detectIndex) : q.length;
            if(q.length > 1 && detectIndex != -1) {
                return q.substring(q.indexOf("=", detectIndex)+1, endIndex);
            } else {
                return "";
            }
        }

        doDetect = getQueryParamValue('detectwmp');


        var addParam = function(name, value) {
           params[name] = value;
        }

        var getParams = function() {
            return params;
        }

        var getParam = function(name) {
            return params[name];
        }

        var getParamTags = function() {
            var paramTags = "";
            for (var param in getParams()) {
                paramTags += '<param name="' + param + '" value="' + getParam(param) + '" />';
            }
            if (paramTags == "") {
                paramTags = null;
            }
            return paramTags;
        }


        var getHTML = function(mov,_w,_h) {

            var wmpHTML = "";
               wmpHTML += '<object   classid="CLSID:22D6F312-B0F6-11D0-94AB-0080C74C7E95" width="' + _w+ '" height="' + _h + '"  id="' + id + '">';
               addParam("src", mov);
                wmpHTML += '<embed type="application/x-mplayer2" pluginspage="http://www.microsoft.com/Windows/MediaPlayer/" src="'
                + mov + '" width="' + _w + '" height="' + _h + '" id="' + id + '"';
                for (var param in getParams()) {
                    wmpHTML += ' ' + param + '="' + getParam(param) + '"';
                }
                wmpHTML += '></embed>';
                if (getParamTags() != null) {
                    wmpHTML += getParamTags();
                }
                wmpHTML += '</object>';
            return wmpHTML;
        }


        var getVariablePairs = function() {
            var variablePairs = new Array();
            for (var name in getVariables()) {
                variablePairs.push(name + "=" + escape(getVariable(name)));
            }
            if (variablePairs.length > 0) {
                return variablePairs.join("&");
            }
            else {
                return null;
            }
        }

        var write = function(elementId,mov,_w,_h) {
            if(isWMPInstalled() || doDetect=='false') {
                if (elementId) {
                    document.getElementById(elementId).innerHTML = _playwmv(mov,_w,_h);
                } else {
                    document.write(_playwmv(mov,_w,_h));
                }
            } else {
                if (redirect != "") {
                    document.location.replace(redirect);
                } else {
                    if (elementId) {
                        document.getElementById(elementId).innerHTML = altTxt +""+ bypassTxt;
                    } else {
                        document.write(altTxt +""+ bypassTxt);
                    }
                }
            }
        }

        var isWMPInstalled = function () {
            var wmpInstalled = false;
            wmpObj = false;
            if (navigator.plugins && navigator.plugins.length) {
                for (var i=0; i < navigator.plugins.length; i++ ) {
                    var plugin = navigator.plugins[i];
                    if (plugin.name.indexOf("Windows Media Player") > -1) {
                        wmpInstalled = true;
                    }
                }
            } else {
                execScript('on error resume next: wmpObj = IsObject(CreateObject("MediaPlayer.MediaPlayer.1"))','VBScript');
                wmpInstalled = wmpObj;
            }
            return wmpInstalled;
        }



         function _playwmv(source,w,h){
            var _w,_h
            if(w && w!=undefined && w>0){
                _w = w;
            }else {
                _w = width;
            }

            if(h && h!=undefined && h>0){
                _h= h;
            }else {
                _h = height;
            }

            currentlyPlaying =source;

            return  getHTML(source,_w,_h);
        }



        function getHTMLOld(source,_w,_h) {
            var _area = '<object id="' + id+ '"  classid="clsid:22d6f312-b0f6-11d0-94ab-0080c74c7e95"'
                +'type="application/x-oleobject" width="'+_w+'"  height="'+_h+'" >'
                +'<param name="showControls" value="1">'
                +'<param name="ShowStatusBar" value="1">'
                +'<param name="fileName" value="'+source+'">'
                +'<embed type="application/x-mplayer2" width="'+_w+'"  height="'+_h+'"   '
                +'showcontrols="false"  ShowStatusBar=\"1\" src="'+source+'"><\/embed><\/object>'
            return _area;
        }




        var _player = null;
        var getPlayer = function (force) {
            if(force ||  YAHOO.lang.isNull(_player)){


                _player = Dom.get(id);

              //  alert('player is'+_player)
                //alert(_player);
                return _player;
            }

            return _player;
        }



        var play= function (source) {
            var _p = getPlayer();
            var _p2 = document.getElementById(id);
             alert(_p.URL );
            alert(_p.controls);

           _p.controls.stop();
            _p.URL = source;
            _p.source= source;
            _p.controls.play();
        }

        
        return {
            currentlyPlaying:currentlyPlaying,
            playwmv:_playwmv,
            play:play,
           id:id,
            addParam:addParam,
            getParams:getParams,
            getParam:getParam,
            getVariablePairs:getVariablePairs,
            write:write,
            isWMPInstalled:isWMPInstalled,
            getQueryParamValue:getQueryParamValue
        // public
        };
    };

})();
YAHOO.register('amostudio.WMPlayer', YAHOO.amostudio.WMPlayer, {
    version: "0.1",
    build: '1'
});