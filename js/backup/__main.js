YAHOO.namespace( 'despani.app' );

YAHOO.despani.app= function(){
	var id="despani";
    var inboxLoaded=false;
    var inboxLoading=false;
    var feedURL='http:/'+'/rss.groups.yahoo.com/group/ydn-javascript/rss?count=50';
    var getFeed=function(u) {
        if (!YAHOO.despani.app.inboxLoading) {
            var reload = true;
            YAHOO.despani.app.inboxLoading = true;
            if (u) {
                if (YAHOO.despani.app.feedURL === (u + '?count=50')) {
                    reload = false;
                }
                YAHOO.despani.app.feedURL = u + '?count=50';
            }
            YAHOO.util.Dom.addClass(YAHOO.despani.app.tabView._tabParent, 'loading');            
            if (!YAHOO.despani.app.inboxLoaded) {
                var transactionObj = YAHOO.util.Get.script('assets/js/inbox.js', { autopurge: true });
            } else {
                if (reload) {
                    YAHOO.despani.app.reloadData(u);
                } else {
                    YAHOO.util.Dom.removeClass(YAHOO.despani.app.tabView._tabParent, 'loading');            
                    YAHOO.despani.app.inboxLoading = false;
                }
            }
        }
    }
  

   //Call loader the first time
    var loader = new YAHOO.util.YUILoader({
        base: 'lib/YahooUI/',
        //Get these modules
        require: ['reset-fonts-grids', 'utilities', 'logger', 'button', 'container', 'tabview', 'selector', 'resize', 'layout'],
        rollup: true,
		scope:this,
        onSuccess:function(){
  
            //Use the DD shim on all DD objects
            YAHOO.util.DDM.useShim = true;
            //Load the global CSS file.
            YAHOO.log('Main files loaded..', 'info', 'main.js');
            YAHOO.util.Get.css('assets/css/example1.css');

            YAHOO.log('Create the first layout on the page', 'info', 'main.js');
            YAHOO.despani.app.layout = new YAHOO.widget.Layout({
                minWidth: 1000,
                units: [
                    { position: 'top', height: 45, resize: false, body: 'top1' },
                    { position: 'right', width: 300, body: '', header: 'Logger Console', collapse: true },
                    { position: 'left', width: 190, resize: true, body: 'left1', gutter: '0 5 0 5px', minWidth: 190 },
                    { position: 'center', gutter: '0 5px 0 2' }
                ]
            });
	
            //On resize, resize the left and right column content
            YAHOO.despani.app.layout.on('resize', function() {
                var l = this.getUnitByPosition('left');
                var th = l.get('height') - YAHOO.util.Dom.get('folder_top').offsetHeight;
                var h = th - 4; //Borders around the 2 areas
                h = h - 9; //Padding between the 2 parts
                YAHOO.util.Dom.setStyle('folder_list', 'height', h + 'px');
            }, YAHOO.despani.app.layout, true);
            //On render, load tabview.js and button.js
            YAHOO.despani.app.layout.on('render', function() {
                window.setTimeout(function() {
                    YAHOO.util.Get.script('assets/js/logger.js');
                    YAHOO.util.Get.script('assets/js/tabview.js'); 
                    YAHOO.util.Get.script('assets/js/buttons.js');
                    YAHOO.util.Get.script('assets/js/calendar.js');
                }, 0);
                YAHOO.despani.app.layout.getUnitByPosition('right').collapse();
                setTimeout(function() {
                    YAHOO.util.Dom.setStyle(document.body, 'visibility', 'visible');
                    YAHOO.despani.app.layout.resize();
                }, 1000);
            });
            //Render the layout
            YAHOO.despani.app.layout.render();
            //Setup the click listeners on the folder list
            YAHOO.util.Event.on('folder_list', 'click', function(ev) {
                var tar = YAHOO.util.Event.getTarget(ev);
                if (tar.tagName.toLowerCase() != 'a') {
                    tar = null;
                }
                //Make sure we are a link in the list's 
                if (tar && YAHOO.util.Selector.test(tar, '#folder_list ul li a')) {
                    //if the href is a '#' then select the proper tab and change it's label
                    if (tar && tar.getAttribute('href', 2) == '#') {
                        YAHOO.util.Dom.removeClass(YAHOO.util.Selector.query('#folder_list li'), 'selected');
                        var feedName = tar.parentNode.className;
                        YAHOO.util.Dom.addClass(tar.parentNode, 'selected');
                        YAHOO.util.Event.stopEvent(ev);
                        var title = tar.innerHTML;
                        var t = YAHOO.despani.app.tabView.get('tabs');
                        for (var i = 0; i < t.length; i++) {
                            if (t[i].get('id') == 'inboxView') {
                                t[i].set('label', title);
                                var u = false;
                                if (feedName.indexOf('-') != -1) {
                                    u = 'http:/'+'/rss.groups.yahoo.com/group/' + feedName + '/rss';
                                }
                                if (feedName.indexOf('inbox') != -1) {
                                    u = 'http:/'+'/rss.groups.yahoo.com/group/ydn-javascript/rss';
                                }
                                YAHOO.despani.app.getFeed(u);
                                YAHOO.despani.app.tabView.set('activeTab', t[i]);
                            }
                        }
                    }
                }
           });
            
            //Create a SimpleDialog used to mimic an OS dialog
            var panel = new YAHOO.widget.SimpleDialog('alert', {
                fixedcenter: true,
                visible: false,
                modal: true,
                width: '300px',
                constraintoviewport: true, 
                icon: YAHOO.widget.SimpleDialog.ICON_WARN,
                buttons: [
                    { text: 'OK', handler: function() {
                        panel.hide();
                    }, isDefault: true }
                ]
            });
            //Set the header
            panel.setHeader('Alert');
            //Give the body something to render with
            panel.setBody('Notta');
            //Render the Dialog to the body
            panel.render(document.body);

            //Create a namepaced alert method
            YAHOO.despani.app.alert = function(str) {
                YAHOO.log('Firing panel setBody with string: ' + str, 'info', 'main.js');
                //Set the body to the string passed
                panel.setBody(str);
                //Set an icon
                panel.cfg.setProperty('icon', YAHOO.widget.SimpleDialog.ICON_WARN);
                //Bring the dialog to the top
                panel.bringToTop();
                //Show it
                panel.show();
            };
            YAHOO.despani.app.alert('This is not a new product from Yahoo! or YUI, just a demonstration of how YUI components can work in concert in the context of a more complex application.');
		  }
		});
    loader.insert();	
  
  var publicVar = 2;
  function publicFunction(){
    anotherPublicFunction();	  
  }
  function anotherPublicFunction(){
    privateFunction();
  }
  function getCurrentState(){
    return 2;
  }
  // პაბლიც და პრაივეტ მეთოდების და პროპერტების გამოქვეყნება ხდება ამ ანონიმურ ბლოკში
  return {
    count:publicVar,
    increase:anotherPublicFunction,
    current:getCurrentState()
  };
}();