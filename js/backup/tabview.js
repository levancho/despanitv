(function() {
    var Dom = YAHOO.util.Dom,
        Event = YAHOO.util.Event,
        Sel = YAHOO.util.Selector;
        YAHOO.log('tabview.js loaded', 'info', 'tabview.js');
        //Set the time on the home screen
        YAHOO.despani.app.setTime = function() {
            var d = new Date();
            var weekday = ['კვირა','ორშაბათი','სამშაბათი','ოთხშაბათი','ხუთშაბათი','პარასკევი','შაბათი'];
            var h = d.getHours(), a = 'am';
            if (h >= 12) {
                a = 'pm';
                if (h > 12) {
                    h = (h - 12);
                }
            }

            var dy = d.getDate();
            if (dy < 10) {
                dy = '0' + dy;
            }

            var m = (d.getMonth() + 1);
            if (m < 10) {
                m = '0' + m;
            }

            var dt = weekday[d.getDay()] + ' ' + m + '/' + dy + '/' + d.getFullYear() + ' ' + h + ':' + d.getMinutes() + ' ' + a;
            YAHOO.util.Dom.get('datetime').innerHTML = dt;
           // YAHOO.util.Dom.get('calDateStr').innerHTML = m + '/' + dy + '/' + d.getFullYear();
            YAHOO.log('Setting the time/date string to: ' + dt, 'info', 'tabview.js');
        };
        
        //Method to Resize the tabview
        YAHOO.despani.app.resizeTabView = function() {
            var ul = YAHOO.despani.app.tabView._tabParent.offsetHeight;
            Dom.setStyle(YAHOO.despani.app.tabView._contentParent, 'height', ((YAHOO.despani.app.layout.getSizes().center.h - ul) - 2) + 'px');
        };
        
        //Listen for the layout resize and call the method
        YAHOO.despani.app.layout.on('resize', YAHOO.despani.app.resizeTabView);
        //Create the tabView
        YAHOO.log('Creating the main TabView instance', 'info', 'tabview.js');
        YAHOO.despani.app.tabView = new YAHOO.widget.TabView("maintabs");
        //Create the Home tab       
        YAHOO.despani.app.tabView.addTab( new YAHOO.widget.Tab({
            //Inject a span for the icon
            label: '<span></span>Home',
            id: 'homeView',
            content: '<div id="welcomeWrapper"><h2>Welcome to the home screen</h2><span id="datetime"></span><div id="weather"><span><em></em><strong>Sunnyvale, CA</strong></span></div></div><div id="news" class="yui-navset"><ul class="yui-nav"><li class="selected" id="newsTop"><a href="#tab1"><em>Top Stories</em></a></li><li id="newsWorld"><a href="#tab2"><em>World</em></a></li><li id="newsEnt"><a href="#tab3"><em>Entertainment</em></a></li><li id="newsSports"><a href="#tab4"><em>Sports</em></a></li></ul><div class="yui-content"><div></div><div></div><div></div><div></div></div></div>',
            active: true
        }));
        //Create the Inbox tab
        YAHOO.despani.app.tabView.addTab( new YAHOO.widget.Tab({
            //Inject a span for the icon
            label: '<span></span>Inbox',
            id: 'inboxView',
            content: ''

        }));
        YAHOO.despani.app.tabView.on('activeTabChange', function(ev) {
            //Tabs have changed
            if (ev.newValue.get('id') == 'inboxView') {
                //inbox tab was selected
               // do nothing
            }
            //Is an editor present?
            if (YAHOO.despani.app.editor) {
                if (ev.newValue.get('id') == 'composeView') {
                    YAHOO.log('Showing the ediitor', 'info', 'tabview.js');
                    YAHOO.despani.app.editor.show();
                    YAHOO.despani.app.editor.set('disabled', false);
                } else {
                    YAHOO.log('Hiding the editor', 'info', 'tabview.js');
                    YAHOO.despani.app.editor.hide();
                    YAHOO.despani.app.editor.set('disabled', true);
                }
            }
            //Resize to fit the new content
            YAHOO.despani.app.layout.resize();
        });
        //Add the tabview to the center unit of the main layout
        var el = YAHOO.despani.app.layout.getUnitByPosition('center').get('wrap');
        YAHOO.despani.app.tabView.appendTo(el);

        //resize the TabView
        YAHOO.despani.app.resizeTabView();
        //Set the time on the home screen
        YAHOO.despani.app.setTime();
        //Setup the interval to update the time
        setInterval(YAHOO.despani.app.setTime, 60000);

        
        YAHOO.log('Fetch the news feed', 'info', 'tabview.js');
        YAHOO.util.Get.script('js/news.js'); 


        //When inboxView is available, update the height..
        Event.onAvailable('inboxView', function() {
            var t = YAHOO.despani.app.tabView.get('tabs');
            for (var i = 0; i < t.length; i++) {
                if (t[i].get('id') == 'inboxView') {
                    var el = t[i].get('contentEl');
                    el.id = 'inboxHolder';
                    YAHOO.log('Setting the height of the TabViews content parent', 'info', 'tabview.js');
                    Dom.setStyle(el, 'height', Dom.getStyle(YAHOO.despani.app.tabView._contentParent, 'height'));
                    
                }
            }

        });

})();
