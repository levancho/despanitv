YAHOO.namespace( 'despani.app' );
YAHOO.namespace( 'despani.data' );
(function() {


    //Call loader the first time
    var loader = new YAHOO.util.YUILoader({
        base: 'lib/YahooUI/',
        //Get these modules
        require: ['reset-fonts-grids', 'utilities', 'logger', 'button', 'container', 'tabview', 'selector', 'resize', 'layout','animation','menu'],
		
        rollup: true,
        onSuccess: function() {
			
		
			
            //Use the DD shim on all DD objects
            YAHOO.util.DDM.useShim = true;
            //Load the global CSS file.
            YAHOO.log('Main files loaded..', 'info', 'main.js');
            YAHOO.util.Get.css('css/example1.css');

            YAHOO.log('Create the first layout on the page', 'info', 'main.js');
            YAHOO.despani.app.layout = new YAHOO.widget.Layout({
                minWidth: 1000,
                units: [
                    { position: 'top', height: 45, resize: false, body: 'top1' },
                    { position: 'right', width: 300, body: '', header: 'Logger Console', collapse: true },
                    { position: 'left', scroll: true ,width: 280, resize: true, body: 'left1', gutter: '0 5 0 5px', minWidth: 240 },
                    { position: 'center', gutter: '0 5px 0 2' }
                ]
            });
            //On resize, resize the left and right column content
            YAHOO.despani.app.layout.on('resize', function() {
                var l = this.getUnitByPosition('left');
                var th = l.get('height') - YAHOO.util.Dom.get('folder_top').offsetHeight;
                var h = th - 4; //Borders around the 2 areas
                h = h - 9; //Padding between the 2 parts
                YAHOO.util.Dom.setStyle('folder_list', 'height', h + 'px');
            }, YAHOO.despani.app.layout, true);
            //On render, load tabview.js and button.js
            YAHOO.despani.app.layout.on('render', function() {
                window.setTimeout(function() {

			
				
                    YAHOO.util.Get.script('js/logger.js');
                    YAHOO.util.Get.script('js/tabview.js'); 
                    YAHOO.util.Get.script('js/buttons.js');
					//YAHOO.util.Get.script('js/chanels.js');
					
					YAHOO.util.Get.script('lib/bubble/bubbling/bubbling.js');
					YAHOO.util.Get.script('lib/bubble/accordion/accordion.js');


                 var successHandler = function(o){
                //Here, o contains all of the fields described in the table
                //above:
                YAHOO.log("DESPANI: "+this.data.chanels); //the data you passed in your configuration

                var rustaviInfo = this.data.chanels.rustavi2.categories[0];

           //alert("DESPANI: "+rustaviInfo.cat);
              // alert("DESPANI: "+rustaviInfo.data.list[0].label);



		window.setTimeout(function() {
                }, 300);
            }

            var objTransaction = YAHOO.util.Get.script("data/chanels.json", {
                onSuccess: successHandler,
                 scope: YAHOO.despani
            });


                }, 0);
                YAHOO.despani.app.layout.getUnitByPosition('right').collapse();
                setTimeout(function() {
                    YAHOO.util.Dom.setStyle(document.body, 'visibility', 'visible');
                    YAHOO.despani.app.layout.resize();
                }, 1000);
            });
            //Render the layout
            YAHOO.despani.app.layout.render();
            //Setup the click listeners on the folder list
            YAHOO.util.Event.on('folder_list', 'click', function(ev) {
                
            });
            
            //Create a SimpleDialog used to mimic an OS dialog
            var panel = new YAHOO.widget.SimpleDialog('alert', {
                fixedcenter: true,
                visible: false,
                modal: true,
                width: '300px',
                constraintoviewport: true, 
                icon: YAHOO.widget.SimpleDialog.ICON_WARN,
                buttons: [
                    { text: 'OK', handler: function() {
                        panel.hide();
                    }, isDefault: true }
                ]
            });
            //Set the header
            panel.setHeader('Alert');
            //Give the body something to render with
            panel.setBody('Notta');
            //Render the Dialog to the body
            panel.render(document.body);

            //Create a namepaced alert method
            YAHOO.despani.app.alert = function(str) {
                YAHOO.log('Firing panel setBody with string: ' + str, 'info', 'main.js');
                //Set the body to the string passed
                panel.setBody(str);
                //Set an icon
                panel.cfg.setProperty('icon', YAHOO.widget.SimpleDialog.ICON_WARN);
                //Bring the dialog to the top
                panel.bringToTop();
                //Show it
                panel.show();
            };
			
			

            YAHOO.despani.app.alert('იტვირთება დესპანის ტელევიზია, გთხოვთ მოიცადოთ ....');
			 window.setTimeout(function() {
                   panel.hide();
                }, 2000);
        }
    });
	
		
    loader.insert();
})();
