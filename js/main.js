
(function() {
    YAHOO.namespace( 'despani.app' );
    YAHOO.namespace( 'despani.data' );
    YAHOO.namespace( 'despani.player' );
     
    YAHOO.despani.player = function () {
        
        var wmp = new YAHOO.amostudio.WMedia( "wmpplayer", 400, 350);
        return {
            wmp:wmp,
            flv:null
        };
    }();

    YAHOO.despani.app = function(){

        var id = "despani_TV";
        var _v = "0.0.1";
        
        var loadChanels = function (data,arxi,selector) {
            var queryString = "$.chanels."+arxi+".categories[0].data.list[?(@.id=='"+selector+"')]";
            YAHOO.log("DESPANI: "+data.chanels); //the data you passed in your configuration
            var catInfo = data.chanels.rustavi2.categories[0];
            var lab= catInfo.info;
            var  kurieri =jsonPath(data,queryString);
            var streams = kurieri[0].streams[0];
            var stream  = streams.stream;
            var  label = kurieri[0].label;

            YAHOO.despani.app. showPlayer(stream[0].url);

            var b = new  Array();
            for (var j in stream) {
                var prop =  stream[j];
                b .push({
                    label: prop.title,
                    value:prop.url,
                    onclick: {
                        fn: function(e,data) {
                            YAHOO.despani.player.wmp.play(data.url);
                        // YAHOO.despani.player.wmp.play();
                        },
                        obj:prop
                    }
                });
            }

        var nacilebi = new YAHOO.widget.ButtonGroup({
            id:  "b"+kurieri[0].id,
            name:  "radiofield3",
            container:  "rightdetails"
        });
        nacilebi.addButtons(b);
    }
    //
    return {
        id:id,
        version:_v,
        loadChanels:loadChanels
    };
}();


    YAHOO.despani.app.c = function(){

        //
        return {
            mainArea:YAHOO.util.Dom.get('mainArea'),
            playerArea:YAHOO.util.Dom.get('playerContainer'),
            left:YAHOO.util.Dom.get('leftdetails'),
            right:YAHOO.util.Dom.get('rightdetails'),
            details:YAHOO.util.Dom.get('playdetails')
        };
    }();




    //Call loader the first time
    var loader = new YAHOO.util.YUILoader({
        base: 'lib/YahooUI/',
        //Get these modules
        require: ['reset-fonts-grids', 'logger', 'button', 'container', 'tabview', 'selector', 'resize', 'layout','menu','json'],
		
        rollup: true,
        onSuccess: function() {
			
		
			
            //Use the DD shim on all DD objects
            YAHOO.util.DDM.useShim = true;
            //Load the global CSS file.
            YAHOO.log('Main files loaded..', 'info', 'main.js');
            YAHOO.util.Get.css('css/example1.css');

            YAHOO.log('Create the first layout on the page', 'info', 'main.js');
            YAHOO.despani.app.layout = new YAHOO.widget.Layout({
                minWidth: 1000,
                units: [
                {
                    position: 'top',
                    height: 45,
                    resize: false,
                    body: 'top1'
                },
                {
                    position: 'right',
                    width: 300,
                    body: '',
                    header: 'Logger Console',
                    collapse: true
                },
                {
                    position: 'left',
                    scroll: true ,
                    width: 280,
                    resize: true,
                    body: 'left1',
                    gutter: '0 5 0 5px',
                    minWidth: 240
                },
                {
                    position: 'center',
                    gutter: '0 5px 0 2'
                }
                ]
            });
            //On resize, resize the left and right column content
            YAHOO.despani.app.layout.on('resize', function() {
                var l = this.getUnitByPosition('left');
                var th = l.get('height') - YAHOO.util.Dom.get('folder_top').offsetHeight;
                var h = th - 4; //Borders around the 2 areas
                h = h - 9; //Padding between the 2 parts
                YAHOO.util.Dom.setStyle('folder_list', 'height', h + 'px');
            }, YAHOO.despani.app.layout, true);
            //On render, load tabview.js and button.js
            YAHOO.despani.app.layout.on('render', function() {
                window.setTimeout(function() {

			
				
                    YAHOO.util.Get.script('js/logger.js');
                    YAHOO.util.Get.script('js/tabview.js'); 
                    YAHOO.util.Get.script('js/buttons.js');
                    //YAHOO.util.Get.script('js/chanels.js');
					
                    YAHOO.util.Get.script('lib/bubble/bubbling/bubbling.js');
                    YAHOO.util.Get.script('lib/bubble/accordion/accordion.js');
                //  YAHOO.util.Get.script('lib/wmpObject.js');
                }, 0);
                YAHOO.despani.app.layout.getUnitByPosition('right').collapse();
                setTimeout(function() {
                    YAHOO.util.Dom.setStyle(document.body, 'visibility', 'visible');
                    YAHOO.despani.app.layout.resize();
                }, 1000);
            });
            //Render the layout
            YAHOO.despani.app.layout.render();
            //Setup the click listeners on the folder list
            YAHOO.util.Event.on('folder_list', 'click', function(ev) {
                
                });
            
            //Create a SimpleDialog used to mimic an OS dialog
            var panel = new YAHOO.widget.SimpleDialog('alert', {
                fixedcenter: true,
                visible: false,
                modal: true,
                width: '300px',
                constraintoviewport: true, 
                icon: YAHOO.widget.SimpleDialog.ICON_WARN,
                buttons: [
                {
                    text: 'OK',
                    handler: function() {
                        panel.hide();
                    }, 
                    isDefault: true
                }
                ]
            });
            //Set the header
            panel.setHeader('Alert');
            //Give the body something to render with
            panel.setBody('Notta');
            //Render the Dialog to the body
            panel.render(document.body);

            //Create a namepaced alert method
            YAHOO.despani.app.alert = function(str) {
                YAHOO.log('Firing panel setBody with string: ' + str, 'info', 'main.js');
                //Set the body to the string passed
                panel.setBody(str);
                //Set an icon
                panel.cfg.setProperty('icon', YAHOO.widget.SimpleDialog.ICON_WARN);
                //Bring the dialog to the top
                panel.bringToTop();
                //Show it
                panel.show();
            };
			
      

            YAHOO.despani.app.alert('იტვირთება დესპანის ტელევიზია, გთხოვთ მოიცადოთ ....');
            window.setTimeout(function() {
                panel.hide();
            }, 2000);
        }
    });
	
		
    loader.insert();
})();
