(function() {
    var Dom = YAHOO.util.Dom,
    Event = YAHOO.util.Event,
    Sel = YAHOO.util.Selector;
    YAHOO.log('tabview.js loaded', 'info', 'tabview.js');
    //Set the time on the home screen
    
    var $c = YAHOO.despani.app.c ;
    var  mainArea = $c .mainArea;
    var  left = $c .left;
    var  right = $c .right;
    var  details = $c .details;
    var wmpPlayer = YAHOO.despani.player.wmp;
    YAHOO.despani.app.showPlayer = function (filename) {
             if(!filename || filename == undefined || filename=="")filename= YAHOO.despani.player.wmp.currentlyPlaying;
            var ww=((YAHOO.despani.app.layout.getSizes().center.w ) - 200);
            var hh=((YAHOO.despani.app.layout.getSizes().center.h ) - 180);
            Dom.setStyle(left, 'width', (ww/2-4) + 'px');
            Dom.setStyle(right, 'width', (ww/2-4) + 'px');
            Dom.setStyle(mainArea, 'height', hh + 'px');
            Dom.setStyle(mainArea, 'width', ww + 'px');
            Dom.setStyle(details, 'width', ww + 'px');

           // alert("filename "+filename);
          //  var a =YAHOO.despani.player.wmp.createPlayer("playerContainer",ww,hh);
            //playerArea.innerHTML =  a

          if(!YAHOO.lang.isUndefined(filename)) {
                    while ($c.playerArea.childNodes.length >= 1) {
                          $c.playerArea.removeChild($c.playerArea.firstChild);
                    }
                    YAHOO.despani.player.wmp.createPlayer("playerContainer",filename,ww,hh);
            }
           //  alert("playerArea  "+ YAHOO.despani.player.wmp.playwmv);
        }



    if(!wmpPlayer.isWMPInstalled()){
        window.setTimeout(function() {

            if(YAHOO.env.ua.gecko){
                YAHOO.despani.app.alert('თქვენს Firefox ბროუზერში,  არ აყენია ვინდოუს მედია ფლეიერის პლაგინი, გთხოვთ დააჭიროთ >> <a href="http://port25.technet.com/videos/downloads/wmpfirefoxplugin.exe" target="_blank" >შემდეგ ბმულს</a> << და ჩატვირთოთ პლაგინი. პლაგინის დაყენების  შემდეგ, საჭიროა  ბროუზერის გადატვირთვა....');
                playerArea.innerHTML='<h1 style="color:red;font-size:20px">ტელევიზია არ იმუშავებს რადგან, თქვენს Firefox ბროუზერში,  არ აყენია ვინდოუს მედია ფლეიერის პლაგინი, გთხოვთ დააჭიროთ >> <a href="http://port25.technet.com/videos/downloads/wmpfirefoxplugin.exe" target="_blank" >შემდეგ ბმულს</a> << და ჩატვირთოთ პლაგინი. პლაგინის ჩამოწერის შემდეგ,მონახეთ ჩამოწერილი ფაილი, და ორჯერ დააწკაპუნეთ ფაილს, რის შემდეგაც აირჩიეთ  ღილაკი "Run" ან "Open".  ზოგ  შემთხვევაში, საჭიროა ფაიერფოქსის ბროუზერის დახურვა,სამან პლაგინის ჩაწერას დაიწყებთ</h1>';
            } else if (YAHOO.env.ua.opera){
                YAHOO.despani.app.alert('თქვენს Opera ბროუზერში,  არ აყენია ვინდოუს მედია ფლეიერის პლაგინი, გთხოვთ დააჭიროთ >> <a href="http://port25.technet.com/videos/downloads/wmpfirefoxplugin.exe" target="_blank" >შემდეგ ბმულს</a> << და ჩატვირთოთ პლაგინი. პლაგინის დაყენების  შემდეგ, საჭიროა  ბროუზერის გადატვირთვა....');
            } else if (YAHOO.env.ua.webkit){
                YAHOO.despani.app.alert('თქვენს Safari  ბროუზერში,  არ აყენია ვინდოუს მედია ფლეიერის პლაგინი, გთხოვთ დააჭიროთ >> <a href="http://port25.technet.com/videos/downloads/wmpfirefoxplugin.exe" target="_blank" >შემდეგ ბმულს</a> << და ჩატვირთოთ პლაგინი. პლაგინის დაყენების  შემდეგ, საჭიროა  ბროუზერის გადატვირთვა....');
            }
        }, 1000);
    } 
    var successHandler = function(o){
      YAHOO.despani.app.loadChanels(this.data,'rustavi2','kurieri');
       // YAHOO.despani.app.loadChanels(this.data,'gpb','pirdapiri');
    }

    var objTransaction = YAHOO.util.Get.script("data/chanels.json", {
        onSuccess: successHandler,
        scope: YAHOO.despani
    });

   
 
    //Method to Resize the tabview
    YAHOO.despani.app.resizeTabView = function() {

             
        var ul = YAHOO.despani.app.tabView._tabParent.offsetHeight;
        Dom.setStyle(YAHOO.despani.app.tabView._contentParent, 'height', ((YAHOO.despani.app.layout.getSizes().center.h - ul) - 2) + 'px');
        //alert("before"+mainArea.style.height);
        YAHOO.despani.app.showPlayer();
    };

    //alert(YAHOO.despani.app.layout.getSizes().center.h);
        
    //Listen for the layout resize and call the method
    YAHOO.despani.app.layout.on('resize', YAHOO.despani.app.resizeTabView);
    //Create the tabView
    YAHOO.log('Creating the main TabView instance', 'info', 'tabview.js');
    YAHOO.despani.app.tabView = new YAHOO.widget.TabView("maintabs");
    //Create the Home tab
   
    //Add the tabview to the center unit of the main layout
    var el = YAHOO.despani.app.layout.getUnitByPosition('center').get('wrap');
    YAHOO.despani.app.tabView.appendTo(el);
    //resize the TabView
    YAHOO.despani.app.resizeTabView();
})();
