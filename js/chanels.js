YAHOO.util.Event.onAvailable("rendertarget", function () {
		
    var Dom = YAHOO.util.Dom, Event = YAHOO.util.Event;
    
    //Create this loader instance and ask for the Button module
    var loader = new YAHOO.util.YUILoader({
        base: 'lib/YahooUI/',
        require: ['menu'],
        onSuccess: function(){
           var oMenu = new YAHOO.widget.Menu("menuwithgroups",{  
                                            position: "static",  
                                            hidedelay:  750 });
      
            /*
             Since this Menu instance is built completely from script, call the
             "render" method passing in the DOM element that it should be
             appended to.
             */
          
            
            var successHandler = function(o){
                //Here, o contains all of the fields described in the table
                //above:
                YAHOO.log("DESPANI: "+o.data.oMenu); //the data you passed in your configuration
                YAHOO.log("DESPANI: "+this.data.chanels); //the data you passed in your configuration
                YAHOO.log("DESPANI: "+o.data.oData); //the data you passed in your configuration
                /*
             Add items to the Menu instance by passing an array of object literals
             (each of which represents a set of YAHOO.widget.MenuItem
             configuration properties) to the "addItems" method.
             */
			var oMenu =o.data.oMenu;
	           
	            
	            
	            // Add the title for each group of menu items
				var i=0;
	            var menuItems = new Array();
				
	             for (var j in this.data.chanels) {
				     var prop = this.data.chanels[j];
				     YAHOO.log("DESP "+prop.title);
					 menuItems[i]=prop.list;
				 	 oMenu.setItemGroupTitle(prop.title, i);
					 i++;
				 }
				 
				oMenu.addItems(menuItems);
				oMenu.render("rendertarget");
				oMenu.show();
				
				window.setTimeout(function() {

				
					
                }, 300);
            }
            
            var objTransaction = YAHOO.util.Get.script("data/chanels.json", {
                onSuccess: successHandler,
                data: {
                    oMenu: oMenu
                }, scope: YAHOO.despani
            });
            
            
            
        }
    });
    //Call insert, only choosing the JS files, so the skin doesn't over write my custom css
    loader.insert({}, 'js');
	
});
